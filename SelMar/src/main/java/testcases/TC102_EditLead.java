package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import design.ProjectMethods;
import pages.createlead.LoginPage;

public class TC102_EditLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		excelfilename = "EditLead";
	}
	
	@Test(dataProvider = "fetchData")
	public void editLead(String userName,String password, String pNumber, String fName) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.clickPhone()
		.enterPhoneNumber(pNumber)
		.clickFindLeads()
		.clickLeadListFirstValue()
		.clickEditButton()
		.updateFirstName(fName)
		.clickUpdateButton()
		.verifyFirstNameInViewLeadPage();
	}
}
