package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import design.ProjectMethods;
import pages.createlead.CreateLeadHomePage;
import pages.createlead.CRMSFAPage;
import pages.createlead.LoginPage;
import pages.createlead.MyHome;
import pages.createlead.MyLeadsPage;
import pages.createlead.ViewLead;

public class TC101_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		excelfilename = "CreateLead";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(String userName, String password, String cName, String fName, String lName) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickSubmitButton()
		.displayFirstName();
	}
	
}






