package pages.editlead;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import design.ProjectMethods;
import pages.createlead.ViewLead;

public class FindLeadHomePage extends ProjectMethods {
	
	public FindLeadHomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//span[text()='Phone']")
	WebElement eleClickPhone;
	
	@FindBy(how = How.XPATH, using = "//input[@name='phoneNumber']")
	WebElement elePhoneNumber;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")
	WebElement eleClickFindLeads;
	
	@FindBy(how = How.XPATH, using = "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleLeadListFirstValue;
	
	public FindLeadHomePage clickPhone() {
		eleClickPhone.click();
		return this;
	}
	
	@Test(dataProvider="fetchData")
	public FindLeadHomePage enterPhoneNumber(String pNumber) {
		elePhoneNumber.sendKeys(pNumber);
		return this;
	}
	
	public FindLeadHomePage clickFindLeads() {
		eleClickFindLeads.click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		return this;
	}
	
	public ViewLead clickLeadListFirstValue() {
		eleLeadListFirstValue.click();
		return new ViewLead();
	}
	
	
}
