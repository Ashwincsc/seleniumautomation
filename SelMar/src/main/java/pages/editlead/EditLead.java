package pages.editlead;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import design.ProjectMethods;
import pages.createlead.ViewLead;

public class EditLead extends ProjectMethods {

	public EditLead() {

		PageFactory.initElements(driver, this); 
	}


	@FindBy(how = How.ID, using = "updateLeadForm_firstName")
	WebElement eleFirstName;

	@FindBy(how = How.CLASS_NAME, using = "smallSubmit")
	WebElement eleUpdateButton;

	@Test(dataProvider="fetchData")
	public EditLead updateFirstName(String fName) {
		eleFirstName.clear();
		eleFirstName.sendKeys(fName);
		return this;
	}

	public ViewLead clickUpdateButton() {
		eleUpdateButton.click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		return new ViewLead();
	}
}
