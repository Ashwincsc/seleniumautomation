package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import design.ProjectMethods;

public class MyHome extends ProjectMethods {

	public MyHome() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Leads")
	WebElement eleLeads;

	public MyLeadsPage clickLeads() {
		eleLeads.click();
		return new MyLeadsPage();

	}

}
