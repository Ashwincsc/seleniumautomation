package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import design.ProjectMethods;
import pages.editlead.EditLead;

public class ViewLead extends ProjectMethods {

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "viewLead_firstName_sp")
	WebElement eleFName;

	@FindBy(how = How.LINK_TEXT, using = "Edit")
	WebElement eleClickEditButton;
	
	@FindBy(how = How.ID, using = "viewLead_firstName_sp")
	WebElement eleFirstNameInViewLeadPage;

	@FindBy(how = How.ID, using = "viewLead_firstName_sp")
	WebElement eleViewLeadFirstName;
	
	// TC101_CreateLead
	public void displayFirstName() {

		String firstName = eleFName.getText();
		System.out.println("The first name value in view lead page is --> "+firstName);

		CreateLeadHomePage createLeadHomePage = new CreateLeadHomePage();
		String fName = createLeadHomePage.fName;

		if(fName.equalsIgnoreCase(firstName)) {
			System.out.println("The name is verified: "+fName);
		}else {
			System.out.println("There is a mismatch in the name entered");
		}

		System.out.println("<-- TestCase Completed -->");
	}

	// TC102_EditLead
	public EditLead clickEditButton() {
		eleClickEditButton.click();
		return new EditLead();
	}
	
	// TC102_EditLead
	public void verifyFirstNameInViewLeadPage() {
		
		String text = eleViewLeadFirstName.getText();
		 
		if (text.contains("Babu")) {
			System.out.println("Text matched in View Lead");
		} else {
			System.out.println("Text not matched in View Lead");
		}
	}
}