package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import design.ProjectMethods;

public class LoginPage extends ProjectMethods{
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "username")
	WebElement eleUserName;
	
	@FindBy(how = How.ID, using = "password")
	WebElement elePassword;
	
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	WebElement eleSubmit;

	public LoginPage enterUserName(String userName) {
		eleUserName.clear();
		eleUserName.sendKeys(userName);
		return this;
	}

	public LoginPage enterPassword(String password) {
		elePassword.clear();
		elePassword.sendKeys(password);
		return this;
	}

	public CRMSFAPage clickLogin() {
		eleSubmit.click();
		return new CRMSFAPage();
	}
}
