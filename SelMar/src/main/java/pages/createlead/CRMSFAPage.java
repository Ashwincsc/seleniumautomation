package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import design.ProjectMethods;

public class CRMSFAPage extends ProjectMethods{
	
	public CRMSFAPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using="CRM/SFA")
	WebElement eleCRMSFA;

	
	public MyHome clickCRMSFA() {
		eleCRMSFA.click(); 
		return new MyHome(); 
	}
}
