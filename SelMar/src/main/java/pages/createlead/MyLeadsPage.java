package pages.createlead;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import design.ProjectMethods;
import pages.editlead.FindLeadHomePage;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement eleCreateLead;
	
	@FindBy(how = How.LINK_TEXT, using = "Find Leads")
	WebElement eleFindLead;
	
	// TC101_CreateLead
	public CreateLeadHomePage clickCreateLead() {

		eleCreateLead.click();
		return new CreateLeadHomePage();
	}

	// TC102_EditLead
	public FindLeadHomePage clickFindLead() {
		
		eleFindLead.click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		return new FindLeadHomePage();
	}
}

