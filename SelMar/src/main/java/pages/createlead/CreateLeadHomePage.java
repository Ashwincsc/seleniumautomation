package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import design.ProjectMethods;

public class CreateLeadHomePage extends ProjectMethods {
	
	public String fName;
	
	public CreateLeadHomePage() {
	
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "createLeadForm_companyName")
	WebElement eleCompanyName;
	
	@FindBy(how = How.ID, using = "createLeadForm_firstName")
	WebElement eleFirstName;
	
	@FindBy(how = How.ID, using = "createLeadForm_lastName")
	WebElement eleLastName;
	
	@FindBy(how = How.NAME, using = "submitButton")
	WebElement eleSubmit;

	@Test(dataProvider="fetchData")
	public CreateLeadHomePage enterCompanyName(String cName) {
		
		eleCompanyName.clear();
		eleCompanyName.sendKeys(cName);
		return this;
	}
	
	public CreateLeadHomePage enterFirstName(String fName) {
		
		eleFirstName.clear();
		eleFirstName.sendKeys(fName);
		fName = eleFirstName.getAttribute("value");
		System.out.println("The first name value that is entered is: "+fName);
		return this;
	}
	
	public CreateLeadHomePage enterLastName(String lName) {
		
		eleLastName.clear();
		eleLastName.sendKeys(lName);
		return this;
	}
	
	public ViewLead clickSubmitButton() {
		
		eleSubmit.click();
		return new ViewLead();
	}
}
