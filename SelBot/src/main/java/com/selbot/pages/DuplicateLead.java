package com.selbot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selbot.testng.api.base.Annotations;

public class DuplicateLead extends Annotations {

	public DuplicateLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_firstName")  
	WebElement eleFirstName;
	
	@FindBy(how=How.NAME, using="submitButton")
	WebElement eleCreateLead;
	
	public DuplicateLead enterFirstName() {
		String fName = "Ashwin";
		clearAndType(eleFirstName, fName);
		return this;
	}
	
	public ViewLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new ViewLeadPage();
	}
}
