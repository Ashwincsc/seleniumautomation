package com.selbot.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.selbot.testng.api.base.Annotations;

public class FindLeadPage extends Annotations {
	
	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//span[text()='Find by']/following::span[5]")  
	WebElement eleEmail;
	
	@FindBy(how=How.NAME, using="emailAddress")
	WebElement eleEmailAddress;
	
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']")
	WebElement eleFindLeads;
	
	@FindBy(how=How.XPATH, using="//a[text()='10110']")
	WebElement eleFindLeadsTableFirstValue;
	
	
	
	public FindLeadPage clickEmail() {
		click(eleEmail);
		return this;
	}
	
	public FindLeadPage enterEmailValue() {
		String emailValue = ".com";
		clearAndType(eleEmailAddress, emailValue);
		return this;
	}
	
	public FindLeadPage clickFindLeads() {
		click(eleFindLeads);
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		return this;
	}
	
	public ViewLeadPage clickFirstValue() {
		click(eleFindLeadsTableFirstValue);
		return new ViewLeadPage();
	}

}
