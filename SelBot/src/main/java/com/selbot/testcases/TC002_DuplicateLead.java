package com.selbot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.selbot.pages.LoginPage;
import com.selbot.testng.api.base.Annotations;

public class TC002_DuplicateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_DuplicateLead";
		testcaseDec = "Login, Duplicate Lead and Verify";
		author = "Ashwin";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String pwd, String companyName, String firstName, String lastName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.clickEmail()
		.enterEmailValue()
		.clickFindLeads()
		.clickFirstValue()
		.clickDuplicateLead()
		.enterFirstName()
		.clickCreateLead()
		.close();
	}
	
}
