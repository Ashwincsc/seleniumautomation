package week3.day1;

import org.apache.xmlbeans.impl.xb.xmlconfig.Extensionconfig.Interface;

public class Inheritance_ScooterBike_Subclass {
	
	public void Dio() {
		System.out.println("Subclass - This is Dio");
	}
	
	public void Activa() {
		System.out.println("Subclass - This is Activa");
	}
	
	
public static void main(String[] args) {
		
	Inheritance_ScooterBike_Subclass myScooterBike = new Inheritance_ScooterBike_Subclass();
		
		//Base class functions
		//myScooterBike.changeGear();
		//myScooterBike.applyBrake();
		
		//Sub class functions
		myScooterBike.Dio();
		myScooterBike.Activa();
	}

}
