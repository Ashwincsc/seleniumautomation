package week3.day1;

public class Inheritance_SportsBike_Subclass extends Inheritance_Vehicle_BaseClass {
	
	public void Ducati() {
		System.out.println("Subclass - This is Ducati");
	}
	
	public void Yamaha() {
		System.out.println("Subclass - This is Yamaha");
	}
	
	public void applyBrake() {
		System.out.println("Subclass - The brake is applied");
	}
	
		
	public static void main(String[] args) {
		
		Inheritance_SportsBike_Subclass mySportsBike = new Inheritance_SportsBike_Subclass();
		
		//Base class functions
		mySportsBike.changeGear();
		mySportsBike.applyBrake();
		
		//Sub class functions
		mySportsBike.Ducati();
		mySportsBike.Yamaha();
	}	

}
