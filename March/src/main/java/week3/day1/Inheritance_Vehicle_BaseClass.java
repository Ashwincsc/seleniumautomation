package week3.day1;

public class Inheritance_Vehicle_BaseClass {
	
	public void changeGear() {
		System.out.println("Base class - The gear is changed");
	}
	
	public void applyBrake() {
		System.out.println("Base class - The brake is applied");
	}
	
	public static void main(String[] args) {
		
		//Base class functions
		Inheritance_Vehicle_BaseClass getVehicle = new Inheritance_Vehicle_BaseClass();
		getVehicle.applyBrake();
		
		//Sub class functions
		Inheritance_SportsBike_Subclass getSportsBike = new Inheritance_SportsBike_Subclass();
		getSportsBike.applyBrake();
				
	}

}
