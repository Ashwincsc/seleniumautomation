package week3.day2;

import java.util.ArrayList;
import java.util.List;

public class List_RemoveSecondName {
	
	public static void main(String[] args) {
		
		/* To remove the second entry of John */
		
		List <String> empNames = new ArrayList<>();
		empNames.add("Mark");
		empNames.add("John");
		empNames.add("Sam");
		empNames.add("Sarath");
		empNames.add("John");
		empNames.add("Sasha");
		
		int count = 0;
		
		for(int i = 0 ; i < empNames.size(); i++) {
			if(empNames.get(i).equalsIgnoreCase("John")) {
				count++;
				if(count == 2) {
					empNames.remove(i);
				}
			}
		}
		System.out.println("The final list of emp names are: "+empNames); 	
	}

}
