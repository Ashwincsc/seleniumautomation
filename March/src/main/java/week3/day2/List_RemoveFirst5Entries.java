package week3.day2;

import java.util.ArrayList;
import java.util.List;

public class List_RemoveFirst5Entries {

public static void main(String[] args) {
		
		/* To remove the first 5 items from the available 10 items */
		
		List <String> empNames = new ArrayList<>();
		empNames.add("Madhavan");
		empNames.add("Vijay Sethupathi");
		empNames.add("Dinesh");
		empNames.add("Kathir");
		empNames.add("Ashwin");
		empNames.add("Sasha");
		empNames.add("Simbhu");
		empNames.add("Rajini");
		empNames.add("Vijay");
		empNames.add("Ajith");

		System.out.println("The size of the array is: "+empNames.size());
		
		for(int i=0; i<=empNames.size(); i++) {
			if(i < 5) {
				empNames.remove(0);
			}
		}
		System.out.println("The values after removing the first 5 items are: "+empNames);
	}


}
