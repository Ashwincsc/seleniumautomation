package week3.day2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;



public class List_HighestAvailableBatFromSportsShop {
	
public static void main(String[] args) {
		
		/* To count each character in a given string */
		
		Map<String, Integer> sportsItems = new HashMap<>();
		sportsItems.put("Reebok", 25);
		sportsItems.put("Britannia", 100);
		sportsItems.put("MRF", 30);
		sportsItems.put("Nike", 40);
		sportsItems.put("SG", 20);
		
		System.out.println(sportsItems.entrySet());
		
		int maxQuantity = Collections.max(sportsItems.values());
		System.out.println("The highest quantity is: "+maxQuantity);
		
		for(Entry<String, Integer> eachItem : sportsItems.entrySet()){
			if(eachItem.getValue() == maxQuantity){
				System.out.println("The highest quantity brand is: "+eachItem.getKey());
			}
		}
	}

}
