package week3.day2;

import java.util.ArrayList;
import java.util.List;

public class List_PrintNamesStartingWithS {
	
public static void main(String[] args) {
		
		/* Print only the names that starts with 'S' */
		
		String finalNames = new String();
		List <String> empNames = new ArrayList<>();
		empNames.add("Madhavan");
		empNames.add("Vijay Sethupathi");
		empNames.add("Sekar");
		empNames.add("Kathir");
		empNames.add("Ashwin");
		empNames.add("Sasha");
		empNames.add("Vijay");
		empNames.add("Rajini");
		empNames.add("simbhu");
		empNames.add("Ajith");

		System.out.println("The names that start with 's' are: ");
		
		for(int i=0; i<empNames.size(); i++) {
			/* Only for comparison we are changing the text to lowercase */
			finalNames = empNames.get(i).toLowerCase();
			if(finalNames.startsWith("s")) {
				/* After comparison and before we print, we get the original text (either it starts with upper or lower case) to print it */
				System.out.println(empNames.get(i));
			}
		}

}
}
