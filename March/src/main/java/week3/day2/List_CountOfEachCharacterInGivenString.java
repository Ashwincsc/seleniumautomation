package week3.day2;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class List_CountOfEachCharacterInGivenString {

	public static void main(String[] args) {
		
		/* To count each character in a given string */
		
		String name = "GeeksforGeeks";
		String withoutDuplicateLetter;
		int count = 0;
		Set<Character> charSet =  new LinkedHashSet<>();
		
		name = name.toLowerCase();
		for (int i=0; i<name.length(); i++) {
			charSet.add(name.charAt(i));
		}
		
		System.out.println(charSet);
		
		withoutDuplicateLetter = charSet.toString().replace(',', ' ').replace('[', ' ').replace(']', ' ').replaceAll("\\s", "");
		System.out.println("The duplicate characters that are found from the text '"+name+"': "+withoutDuplicateLetter);
		
		for (int i=0; i<withoutDuplicateLetter.length(); i++) {
			System.out.print(withoutDuplicateLetter.charAt(i));
			for(int j=0; j<name.length(); j++){
				if(withoutDuplicateLetter.charAt(i) == name.charAt(j)){
					count++;
				}
			}
			System.out.println(" --> "+count);
			count = 0;
		}
	}
}
