package week1.day1;

public class CarBrandAndModelSearch {
	
	public static void main (String[] args) {
		
		String brandname = "hyundai";
		
		CarBrandAndModelAvailability carBrandAndModelAvailability = new CarBrandAndModelAvailability();
		carBrandAndModelAvailability.getCarModel(brandname);
	}

}
