package week1.day1;

public class FindMobileColorOfFamily {
	
	String son 		= "Black";
	String daughter = "Pink";
	String dad 		= "Red";
	String mom 		= "Blue";
	
	//NESTED IF EXAMPLE
	public String getMobile(String character) {
	
		if(character.equalsIgnoreCase("Son")) {
			return son;
		}else if(character.equalsIgnoreCase("daughter")) {
			return daughter;	
		}else if(character.equalsIgnoreCase("dad")) {
			return dad;
		}else if(character.equalsIgnoreCase("mom")) {
			return mom;
		}else {
			return "white";
		}
	}
	
	//SWITCH CASE EXAMPLE
	public void getMobileColor(String familymember) {
		switch(familymember) {
		case "son": 
			System.out.println("Son mobile color is blue");
			break;
			
		case "father":
			System.out.println("Father mobile color is red");
			break;
			
		case "mother":
			System.out.println("Mother mobile color is pink");
			break;
			
		default:
			System.out.println("The defult mobile color is white");
			break;
			
		}
	}

}
