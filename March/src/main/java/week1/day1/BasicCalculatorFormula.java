package week1.day1;

public class BasicCalculatorFormula {
	
	public int isAdd(int A, int B) {
		
		int C = A + B;
		return C;
		
	}
	
	public int isSub(int A, int B) {
		
		int C = A - B;
		return C;
		
	}
	
	public int isMul(int A, int B) {
		
		int C = A * B;
		return C;
		
	}
	
	public int isDiv(int A, int B) {
		
		int C = A / B;
		return C;
		
	}

}
