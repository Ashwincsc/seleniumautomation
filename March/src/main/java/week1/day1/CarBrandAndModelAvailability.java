package week1.day1;

public class CarBrandAndModelAvailability {
	
	String hyundai = "Creta, Tucson, i10";
	String toyota  = "Corolla, Fortuner, Endeavour";
	String maruti  = "Omni, Wagnor, Alto";
	
	public String getCarModel(String brandname) {
		
		if(brandname.equalsIgnoreCase("Hyundai")) {
			System.out.println("The models in hyundai are: "+hyundai);
		}else if(brandname.equalsIgnoreCase("Toyota")) {
			System.out.println("The models in toyota are: "+toyota);
		}else if(brandname.equalsIgnoreCase("Maruti")) {
			System.out.println("The models in Maruti are: "+maruti);
		}else {
			System.out.println("This brand: "+brandname+" is not available with us");
		}
		return brandname;
	}

}
