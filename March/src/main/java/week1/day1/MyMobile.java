package week1.day1;

import week1.day2.ChangeEveryOddIndexToUppercase;
import week1.day2.FindOccurenceOfCharacterA;
import week1.day2.PrintOddSelectionNames;
import week1.day2.PrintSumOfNumbersFrom1To100;
import week1.day2.SplitText;

public class MyMobile {
	
	public static void main (String[] args) {
		
		//Invoking a class
		Mobile myMobile = new Mobile();
		FindGreatestBtwThreeNumbers findGreatestBtwThreeNumbers = new FindGreatestBtwThreeNumbers();
		FindMobileColorOfFamily findMobileColorOfFamily = new FindMobileColorOfFamily();
		PrintOddSelectionNames printOddSelectionNames = new PrintOddSelectionNames();
		PrintSumOfNumbersFrom1To100 printSumOfNumbersFrom1To100 = new PrintSumOfNumbersFrom1To100();
		ChangeEveryOddIndexToUppercase changeEveryOddIndexToUppercase = new ChangeEveryOddIndexToUppercase();
		FindOccurenceOfCharacterA findOccurenceOfCharacterA = new FindOccurenceOfCharacterA();
		SplitText splitText = new SplitText();
		
		//Initialization
		String brandName = myMobile.getBrandName();
		int phoneNumber = myMobile.getPhoneNumber();
		myMobile.getPhoneVersion();
		
		//Results - Method
		System.out.println("BrandName: "+ brandName);
		System.out.println("PhoneNumber: "+ phoneNumber);
		
		//Results - Variables
		System.out.println("PhoneName: "+ myMobile.phoneName);
		System.out.println("Locked: "+ myMobile.isLocked);
		
		//Implement nested if else conditions for different mobile colors
		String mobileColor = findMobileColorOfFamily.getMobile("son");
		System.out.println("The mobile color is: "+mobileColor);
		
		//Find greatest number between 3 numbers
		findGreatestBtwThreeNumbers.greatestBetweenThreeNumbers();
		
		//Switch Case Example
		findMobileColorOfFamily.getMobileColor("mother");
		
		//For Each Example
		printOddSelectionNames.displayOddNames();
		
		
		printSumOfNumbersFrom1To100.printSumOfNumbersFrom1To100();
		
		changeEveryOddIndexToUppercase.oddIndexToUpperCase();
		
		findOccurenceOfCharacterA.occurenceOfCharacterA();
		
		splitText.getSplitText();

		

		
	}

}
