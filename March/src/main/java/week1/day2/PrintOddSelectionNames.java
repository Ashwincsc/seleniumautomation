package week1.day2;

public class PrintOddSelectionNames {

	public String[] displayOddNames() {

		String[] txt = new String[6];
		txt[0] = "Gayathri";
		txt[1] = "Sarath";
		txt[2] = "Koushik";
		txt[3] = "Balaji";
		txt[4] = "Mohan";
		txt[5] = "Babu";

		/*
		 * for(String eachTxt : txt) { System.out.println(eachTxt); }
		 */
		for(int i=1; i<txt.length; i++) {
			System.out.println("The odd names are: "+txt[i++]);
		}
		
		return txt;

	}

}
