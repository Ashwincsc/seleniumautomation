package week1.day2;

public class FindOccurenceOfCharacterA {
			
		public int occurenceOfCharacterA()
		{
			String str = "whoiscalledAlbertaofAmerica";
			int count = 0;
		    char x = 'a';
		    char y = 'A';

		    for(int i=0; i < str.length(); i++)
		    {   
		    	if(str.charAt(i) == x || str.charAt(i) == y){
		            count++;
		    	}
			}
		    System.out.println("The total occurence of alphabet '"+x+"' or '"+y+"' is: "+count);
		    return count;
		}
	}
