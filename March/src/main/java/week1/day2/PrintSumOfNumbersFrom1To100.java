package week1.day2;

public class PrintSumOfNumbersFrom1To100 {
	
	public void printSumOfNumbersFrom1To100 () {
		
		int initialValue = 0;
		
		for(int i=0; i<=100; i++) {
			initialValue = i+initialValue;
		}
		System.out.println("The sum of numbers from 1 to 100 is: "+initialValue);
	}

}
