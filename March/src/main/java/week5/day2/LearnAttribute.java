package week5.day2;

import org.testng.annotations.Test;

public class LearnAttribute {

	@Test(enabled = false)
	public void createLead() {
		System.out.println("Create Lead");
		throw new RuntimeException();
	}

	@Test
	public void deleteLead() {
		System.out.println("Delete Lead");
	}

	@Test
	public void editLead() {
		System.out.println("Edit Lead");
	}

	@Test
	public void mergeLead() {
		System.out.println("Merge Lead");
	}

}


/*
 * dependsonMethods - if we have this attribute then it will run always at the
 * last. Create Lead Delete Lead Merge Lead Edit Lead
 * 
 */


/* scenario 2
Two different methods invoking one function
In this scenario it look for alphabetical order for dependsOnMethod attribute

if the dependent method fails then the skip count will be available


Default test
Tests run: 4, Failures: 1, Skips: 1
    
Pass
Fail
Skip
Ignore - It will not print in console

@Test(priority = -1, dependsOnMethods = "createLead")
	public void deleteLead() {
		System.out.println("Delete Lead");
	}
	
	In this scenario - it is not recommended to give like this. Even in this scenario it will neglect the priority attribute and it will consider dependsOnMethod attribute


@Test
	public void createLead() {
		System.out.println("Create Lead");
		throw new RuntimeException();
	}

	@Test(alwaysRun = true, dependsOnMethods = "createLead")
	public void deleteLead() {
		System.out.println("Delete Lead");
	}


In this scenario I do not want to skip deleteLead, so in this case if I give alwaysRun = true then even though createLead fails, the deleteLead will not skip and it will run



@Test(enabled = false)
	public void createLead() {
		System.out.println("Create Lead");
		throw new RuntimeException();
	}
	
	In this scenario, it will skip the createLead since enabled is given as false. THe default value of enabled is true.

*/