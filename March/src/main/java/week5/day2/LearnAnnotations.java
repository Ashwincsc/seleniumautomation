package week5.day2;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LearnAnnotations {

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("Before Suite");
	}

	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("Before Class");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Before Method");
	}

	@Test
	public void atTest() {
		System.out.println("at Test");
	}

	@Test
	public void atTest1() {
		System.out.println("at Test 1");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("After Method");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("After Class");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("After Test");
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("After Suite");
	}

}




/*
 * except @Test all other are optional
 * 
 * @BeforeSuite
 * @BeforeTest
 * @BeforeClass 
 * 
 * The above 3 are one time implementation
 * 
 * Interview Question
 * 
 * Two @BeforeSuite is possible. It will take according to the alphabetical order.
 * 
 * 
 * one @Test
 * One Super Class
 * One Super Class
 * 
 * 
 * 
 * 
 */
