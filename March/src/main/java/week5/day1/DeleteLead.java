package week5.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DeleteLead extends ProjectMethods{
	
	
	@BeforeTest
	public void setData() {
		fileName = "ExcelData";
		sheetName = "DeleteLead";
	}
	
	
	//@Test(enabled = true, dependsOnMethods = "week5.day1.EditLead.eLead")
	@Test(dataProvider = "fetchData")
	public void deleteLead(String eMail) throws InterruptedException{
		
		System.out.println("<-- Delete Lead Starts -->");
		cDriver.findElementByXPath("//a[text()='Leads']").click();
		cDriver.findElementByLinkText("Find Leads").click();
		cDriver.findElementByXPath("//span[text()='Find by']/following::span[5]").click();
		cDriver.findElementByName("emailAddress").sendKeys(eMail);
		cDriver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		
		WebElement leadListTable = cDriver.findElementByXPath("//span[text()='Lead List']/following::div");
		List<WebElement> allRows = leadListTable.findElements(By.tagName("tr"));
		//System.out.println("first row value: " + allRows.get(1).getText());
		
		WebElement firstColumnValue = null;
		
		for (int i = 0; i < allRows.size(); i++) {
			WebElement firstRow = allRows.get(1);
			List<WebElement> allColumns = firstRow.findElements(By.tagName("td"));
			//System.out.println("First Col value is: "+allColumns.get(0).getText());
			for(int j=0; j < allColumns.size(); j++){
				firstColumnValue = allColumns.get(0);
			}
		}
		System.out.println("The first column value is: "+firstColumnValue.getText());
		String firstColumnValueInString = firstColumnValue.getText();
		cDriver.findElement(By.linkText(firstColumnValue.getText())).click();
		
		String pageTitle = cDriver.getTitle();
		if(cDriver.getTitle().equalsIgnoreCase(pageTitle)){
			System.out.println("The page title '"+pageTitle.toUpperCase()+"' is verified");
		}else{
			System.out.println("The title is not verified");
		}
		
		cDriver.findElementByXPath("//a[text()='Delete']").click();
		cDriver.findElementByLinkText("Find Leads").click();
		cDriver.findElementByName("id").sendKeys(firstColumnValueInString);
		cDriver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		String noRecordsFoundMessage_01 = cDriver.findElementByXPath("//span[text()='Lead List']/following::div[contains(text(),'No records to display')]").getText();
		System.out.println("The value displayed in the table is: "+noRecordsFoundMessage_01);
		
		String noRecordsFoundMessage_02 = cDriver.findElementByXPath("//span[text()='Lead List']/following::div[contains(text(),'No records to display')]").getText(); 
		if(noRecordsFoundMessage_01.equalsIgnoreCase(noRecordsFoundMessage_02)){
			System.out.println("The expected value is: "+noRecordsFoundMessage_01.toUpperCase());
		}else{
			System.out.println("The expected value is not displayed");
		}
		System.out.println("<-- Delete Lead Ends -->");
		
}

}
