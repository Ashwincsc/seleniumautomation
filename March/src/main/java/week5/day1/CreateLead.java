package week5.day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead extends ProjectMethods{

	
	/*
	 * @DataProvider(name = "fetchData") public String[][] getData() { String[][]
	 * data = new String[2][3]; data[0][0] = "TestLeaf"; data[0][1] = "Kevin";
	 * data[0][2] = "Peterson";
	 * 
	 * data[1][0] = "TestLeaf"; data[1][1] = "Sachin"; data[1][2] = "Tendulkar";
	 * 
	 * return data; }
	 */
	 
	@BeforeTest
	public void setData() {
		fileName = "ExcelData";
		sheetName = "CreateLead";
	}
	
	
	@Test(dataProvider = "fetchData")
	public void cLead(String cName, String fName, String lName) {

		System.out.println("<-- Create Lead Starts -->");
		cDriver.findElementByLinkText("Leads").click();
		cDriver.findElementByLinkText("Create Lead").click();
		cDriver.findElementById("createLeadForm_companyName").sendKeys(cName);
		cDriver.findElementById("createLeadForm_firstName").sendKeys(fName);
		cDriver.findElementById("createLeadForm_lastName").sendKeys(lName);
		cDriver.findElementByName("submitButton").click();
		System.out.println("<-- Create Lead Ends -->");
	}
}






