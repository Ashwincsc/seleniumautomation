package week5.day1;

import java.io.IOException;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week5.day2.ReadExcel;

public class ProjectMethods {

	public ChromeDriver cDriver;
	public String fileName;
	public String sheetName;
	
	@Parameters({"url", "username", "password"})
	@BeforeMethod
	public void startApp(String siteURL, String userName, String passWord) {

		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		cDriver = new ChromeDriver();

		// Launch a website in the chrome browser
		cDriver.get(siteURL);

		// Maximize the chrome browser
		cDriver.manage().window().maximize();

		cDriver.findElementById("username").sendKeys(userName);
		cDriver.findElementById("password").sendKeys(passWord);
		cDriver.findElementByClassName("decorativeSubmit").click();
		cDriver.findElementByLinkText("CRM/SFA").click();
	}

	@AfterMethod
	public void closeApp() {
		cDriver.quit();
	}
	
	@DataProvider(name = "fetchData")
	public String[][] getDataFromExcel() throws IOException{
		ReadExcel excelData = new ReadExcel();
		return excelData.readExcel(fileName, sheetName);
	}
	
	
	
	
	
	

}
