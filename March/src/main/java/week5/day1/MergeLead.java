package week5.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class MergeLead extends ProjectMethods{
	
	@Test
	public void mLead() throws InterruptedException {
		
				cDriver.findElementByXPath("//a[text()='Leads']").click();
				cDriver.findElementByXPath("//a[text()='Merge Leads']").click();
				cDriver.findElementByXPath("//span[text()='From Lead']/following::img[1]").click();
				
				Set<String> allWindows = cDriver.getWindowHandles();
				List<String> firstWindow = new ArrayList<>(allWindows);
				cDriver.switchTo().window(firstWindow.get(1));
				cDriver.manage().window().maximize();
				
				cDriver.findElementByXPath("//input[@name='id']").sendKeys("103");
				cDriver.findElementByXPath("//button[text()='Find Leads']").click();
				cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				
				WebElement leadListTable = cDriver.findElementByXPath("//div[@class='x-panel-ml']/following::div[@class='x-panel-bwrap ']");
				List<WebElement> allRows = leadListTable.findElements(By.tagName("tr"));
				
				WebElement firstColumn = null;
				
				for(int i=0; i<allRows.size(); i++){
					WebElement firstRow = allRows.get(1);
					//System.out.println("------------- "+firstRow.getText());
					List<WebElement> allColumnsInFirstRow = firstRow.findElements(By.tagName("td"));
					for(int j=0; j<allColumnsInFirstRow.size(); j++){
						firstColumn = allColumnsInFirstRow.get(2);
					}
				}
				
				String firstLeadsFirstName = firstColumn.getText();
				System.out.println("The first row and first column value in FROM LEAD is: "+firstLeadsFirstName);
				cDriver.findElement(By.linkText(firstColumn.getText())).click();
				
				cDriver.switchTo().window(firstWindow.get(0));
				cDriver.findElementByXPath("//span[text()='To Lead']/following::img[1]").click();
				cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				
				Set<String> allWindows1 = cDriver.getWindowHandles();
				List<String> secondWindow = new ArrayList<>(allWindows1);
				cDriver.switchTo().window(secondWindow.get(1));
				cDriver.manage().window().maximize();
				
				cDriver.findElementByXPath("//input[@name='id']").sendKeys("105");
				cDriver.findElementByXPath("//button[text()='Find Leads']").click();
				cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				
				//WebElement leadListTable1 = cDriver.findElementByXPath("//span[text()='Find Leads']/following::div[@class='x-grid3-viewport']");
				WebElement leadListTable1 = cDriver.findElementByXPath("//span[@class='subSectionTitle']/following::div[@class='x-panel-bwrap ']");
				List<WebElement> allRows1 = leadListTable1.findElements(By.tagName("tr"));
				
				WebElement firstColumn1 = null;
				
				for(int i=0; i < allRows1.size(); i++){
					WebElement firstRow1 = allRows1.get(1);
					List<WebElement> allColumnsInFirstRow1 = firstRow1.findElements(By.tagName("td"));
					for(int j=0; j < allColumnsInFirstRow1.size(); j++){
						firstColumn1 = allColumnsInFirstRow1.get(2);
					}
				}
				
				String firstLeadsFirstName1 = firstColumn1.getText();
				System.out.println("The first row and first column value in TO LEAD is: "+firstLeadsFirstName1);
				cDriver.findElement(By.linkText(firstColumn1.getText())).click();
				
				cDriver.switchTo().window(firstWindow.get(0));
				cDriver.findElementByXPath("//a[text()='Merge']").click();
				
				String alertText = cDriver.switchTo().alert().getText();
				System.out.println("The text in the alert is: "+alertText);
				cDriver.switchTo().alert().accept();
				
				cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				
				cDriver.findElementByXPath("//a[text()='Find Leads']").click();
				cDriver.findElementByXPath("//input[@name='id']").sendKeys("@@@");
				cDriver.findElementByXPath("//button[text()='Find Leads']").click();
				
				//cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				Thread.sleep(3000);
				
				String errorMessage = cDriver.findElementByXPath("//div[@class='x-paging-info']").getText().trim();
				
				if(errorMessage.contains("No records to display")){
					System.out.println("The error message is verified");
				}else{
					System.out.println("The error message is not verified");
				}

	}

}
