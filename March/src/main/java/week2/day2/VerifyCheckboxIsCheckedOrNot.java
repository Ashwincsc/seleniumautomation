package week2.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class VerifyCheckboxIsCheckedOrNot {
	
	public static void main(String args[]) {
		
				// To launch a chrome browser
				System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
				ChromeDriver cDriver = new ChromeDriver();
				
				// Launch a website in the chrome browser
				cDriver.get("http://leafground.com/pages/checkbox.html");
				
				// Maximize the chrome browser
				cDriver.manage().window().maximize();
				
				WebElement isCheckBoxSelected = cDriver.findElementByXPath("//label[text()='Confirm Selenium is checked']/following::input[1]");
				
				//Positive Scenario
				if(isCheckBoxSelected.isSelected()) {
					System.out.println("1. It is selected : "+isCheckBoxSelected.isSelected());
				}else {
					System.out.println("2. It is not selected");
				}
				
				//Negative scenario
				isCheckBoxSelected.click();
				
				if(isCheckBoxSelected.isSelected()) {
					System.out.println("3. It is selected : "+isCheckBoxSelected.isSelected());
				}else {
					System.out.println("4. It is not selected");
				}
		
	}

}
