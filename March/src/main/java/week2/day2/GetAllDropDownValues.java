package week2.day2;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class GetAllDropDownValues {
	
public static void main(String args[]){
		
		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		ChromeDriver cDriver = new ChromeDriver();
		
		// Launch a website in the chrome browser
		cDriver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		// Maximize the chrome browser
		cDriver.manage().window().maximize();
		
		WebElement elementCountry = cDriver.findElementById("userRegistrationForm:countries");
		Select countryDropDown = new Select(elementCountry);
		List<WebElement> allCountryNames = countryDropDown.getOptions();
		System.out.println("Below are the country name: "+allCountryNames.size());
		
		for(int i=1; i<allCountryNames.size(); i++){
			//finalValue = allCountryNames.get(i);
			System.out.println(allCountryNames.get(i).getText());
		}
		cDriver.close();
	}

}
