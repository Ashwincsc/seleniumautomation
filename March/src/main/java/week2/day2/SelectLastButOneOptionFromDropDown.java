package week2.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectLastButOneOptionFromDropDown {
	
	public static void main(String args[]) {
		
		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		ChromeDriver cDriver = new ChromeDriver();
		
		// Launch a website in the chrome browser
		cDriver.get("http://leafground.com/pages/Dropdown.html");
		
		// Maximize the chrome browser
		cDriver.manage().window().maximize();
		
		WebElement elementDropDown = cDriver.findElementByXPath("//select[@name='dropdown2']");
		Select dropDownValues = new Select(elementDropDown);
		List<WebElement> allSelectedOptions = dropDownValues.getOptions();
		int dropDownSize = allSelectedOptions.size();
		String lastButOneOption = allSelectedOptions.get(dropDownSize-2).getText();
		System.out.println("The last but one option value from dropdown is: "+lastButOneOption);
		
		cDriver.close();
		
	}

}
