package week2.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AdvancedUserInteractions_Eg01 {
	
	public static void main(String[] args) throws InterruptedException {
	
	// To launch a chrome browser
	System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
	ChromeDriver cDriver = new ChromeDriver();
	
	// Launch a website in the chrome browser
	cDriver.get("https://jqueryui.com/droppable/");
	cDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
	// Maximize the chrome browser
	cDriver.manage().window().maximize();
	cDriver.switchTo().frame(0);
	
	WebElement myElement = cDriver.findElementById("draggable");
	
	Actions builder = new Actions(cDriver);
	builder.clickAndHold(myElement).moveToElement(myElement, 225, 75).perform();
	
	}
}

