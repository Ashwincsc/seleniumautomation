package week2.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {
	
	public static void main(String args[]) throws InterruptedException{
		
				// To launch a chrome browser
				System.setProperty("webdriver.chrome.driver", "C:\\Users\\259412\\workspace\\Ashwin\\SelMar\\drivers\\chromedriver.exe");
				ChromeDriver cDriver = new ChromeDriver();
				
				// Launch a website in the chrome browser
				cDriver.get("http://leaftaps.com/opentaps");
				
				// Maximize the chrome browser
				cDriver.manage().window().maximize();
				
				// Form details
				cDriver.findElementById("username").sendKeys("Demosalesmanager");
				cDriver.findElementById("password").sendKeys("crmsfa");
				cDriver.findElementByClassName("decorativeSubmit").click();
				cDriver.findElementByLinkText("CRM/SFA").click();
				cDriver.findElementByXPath("//a[text()='Leads']").click();
				cDriver.findElementByLinkText("Find Leads").click();
				cDriver.findElementByXPath("//span[text()='Find by']/following::span[5]").click();
				cDriver.findElementByName("emailAddress").sendKeys(".com");
				cDriver.findElementByXPath("//button[text()='Find Leads']").click();
				Thread.sleep(5000);
				
				WebElement leadListTable = cDriver.findElementByXPath("//span[text()='Lead List']/following::div");
				List<WebElement> allRows = leadListTable.findElements(By.tagName("tr"));
				//System.out.println("first row value: " + allRows.get(1).getText());
				
				WebElement firstColumnValue = null;
				
				for (int i = 0; i < allRows.size(); i++) {
					WebElement firstRow = allRows.get(1);
					List<WebElement> allColumns = firstRow.findElements(By.tagName("td"));
					//System.out.println("First Col value is: "+allColumns.get(0).getText());
					for(int j=0; j < allColumns.size(); j++){
						firstColumnValue = allColumns.get(0);
					}
				}
				System.out.println("The first column value is: "+firstColumnValue.getText());
				cDriver.findElement(By.linkText(firstColumnValue.getText())).click();
				
				String pageTitle = cDriver.getTitle();
				if(cDriver.getTitle().equalsIgnoreCase(pageTitle)){
					System.out.println("The page title '"+pageTitle.toUpperCase()+"' is verified");
				}else{
					System.out.println("The title is not verified");
				}
				
				cDriver.findElementByXPath("//a[text()='Edit']").click();
				cDriver.findElementById("updateLeadForm_companyName").clear();
				WebElement companyName = cDriver.findElementByXPath("//input[@id='updateLeadForm_companyName']");
				companyName.sendKeys("Amazon");
				String Company = companyName.getAttribute("value");
				System.out.println("The updated text before clicking update button is: "+Company);
				
				cDriver.findElementByXPath("//input[@value='Update']").click();
				
				WebElement updatedCompanyName = cDriver.findElementByXPath("//span[@id='viewLead_companyName_sp']"); 
				if(updatedCompanyName.getText().contains(Company)){
					System.out.println("The updated company name is displayed correctly: "+updatedCompanyName.getText());
				}else{
					System.out.println("The updated company name is not displayed correctly");
				}
				
				cDriver.close();
				
				
	}

}
