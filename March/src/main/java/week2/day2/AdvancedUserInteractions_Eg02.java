package week2.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AdvancedUserInteractions_Eg02 {
	
	public static void main(String[] args) throws InterruptedException {
		
		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		ChromeDriver cDriver = new ChromeDriver();
		
		// Launch a website in the chrome browser
		cDriver.get("https://jqueryui.com/selectable/");
		cDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		// Maximize the chrome browser
		cDriver.manage().window().maximize();
		cDriver.switchTo().frame(0);
		
		WebElement item2 = cDriver.findElementByXPath("//li[text()='Item 2']");
		WebElement item6 = cDriver.findElementByXPath("//li[text()='Item 6']");
		
		Actions builder = new Actions(cDriver);
		//builder.click(item2).clickAndHold(item6).perform();
		//builder.click(item2).keyDown(item6, Keys.CONTROL).perform();
		builder.keyDown(item2, Keys.COMMAND).clickAndHold(item6).perform(); // I have used Keys.COMMAND because I use Mac OS. If it is a windows OS then it would be Keys.CONTROL
		
		}

}
