package week2.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ERail {
	
	public static void main(String[] args) throws InterruptedException {
		
				// To launch a chrome browser
				System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
				ChromeDriver cDriver = new ChromeDriver();
				
				// Launch a website in the chrome browser
				cDriver.get("https://erail.in/");
				cDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				
				// Maximize the chrome browser
				cDriver.manage().window().maximize();
				
				cDriver.findElementById("txtStationFrom").clear();
				cDriver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
				cDriver.findElementById("txtStationTo").clear();
				cDriver.findElementById("txtStationTo").sendKeys("SA",Keys.TAB);
				
				WebElement checkBoxValue = cDriver.findElementById("chkSelectDateOnly");
				if(checkBoxValue.isSelected()) {
					cDriver.findElementById("chkSelectDateOnly").click();
				}
				Thread.sleep(3000);
				
				WebElement trainTable = cDriver.findElementByXPath("//table[@class='DataTable TrainList']");
				List<WebElement> rows = trainTable.findElements(By.tagName("tr"));
				//System.out.println("The first train is: "+rows.get(0).getText());
				
				// To display the train names alone from the table
				for (int i = 0; i < rows.size(); i++) {
					WebElement firstRow = rows.get(i);
					List<WebElement> columns = firstRow.findElements(By.tagName("td"));
					//System.out.println(columns.get(1).getText());
					
					// To display the train names that starts with "M"
					if(columns.get(1).getText().startsWith("M")) {
						System.out.println(columns.get(0).getText()+" - "+columns.get(1).getText()); 
					}
			
				}
				
	}

}
