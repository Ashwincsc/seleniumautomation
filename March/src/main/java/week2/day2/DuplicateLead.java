package week2.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {
	
	public static void main(String args[]) throws InterruptedException{
		
		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\259412\\workspace\\Ashwin\\SelMar\\drivers\\chromedriver.exe");
		ChromeDriver cDriver = new ChromeDriver();
		
		// Launch a website in the chrome browser
		cDriver.get("http://leaftaps.com/opentaps");
		
		// Maximize the chrome browser
		cDriver.manage().window().maximize();
		
		// Form details
		cDriver.findElementById("username").sendKeys("Demosalesmanager");
		cDriver.findElementById("password").sendKeys("crmsfa");
		cDriver.findElementByClassName("decorativeSubmit").click();
		cDriver.findElementByLinkText("CRM/SFA").click();
		cDriver.findElementByXPath("//a[text()='Leads']").click();
		cDriver.findElementByLinkText("Find Leads").click();
		cDriver.findElementByXPath("//span[text()='Find by']/following::span[5]").click();
		cDriver.findElementByName("emailAddress").sendKeys(".com");
		cDriver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		
		WebElement leadListTable = cDriver.findElementByXPath("//span[text()='Lead List']/following::div");
		List<WebElement> allRows = leadListTable.findElements(By.tagName("tr"));
		//System.out.println("first row value: " + allRows.get(1).getText());
		
		WebElement firstColumnValue = null;
		
		for (int i = 0; i < allRows.size(); i++) {
			WebElement firstRow = allRows.get(1);
			List<WebElement> allColumns = firstRow.findElements(By.tagName("td"));
			//System.out.println("First Col value is: "+allColumns.get(0).getText());
			for(int j=0; j < allColumns.size(); j++){
				firstColumnValue = allColumns.get(2);
			}
		}
		
		String firstLeadsFirstName = firstColumnValue.getText();
		System.out.println("The first column value is: "+firstColumnValue.getText());
		cDriver.findElement(By.linkText(firstColumnValue.getText())).click();
		
		String pageTitle = cDriver.getTitle();
		if(cDriver.getTitle().equalsIgnoreCase(pageTitle)){
			System.out.println("The page title '"+pageTitle.toUpperCase()+"' is verified");
		}else{
			System.out.println("The title is not verified");
		}
		
		Thread.sleep(3000);
		cDriver.findElementByXPath("//div[@class='frameSectionExtra']/following::a[text()='Duplicate Lead']").click();
		
		// To verify negative scenario
		//cDriver.findElementById("createLeadForm_firstName").clear();
		//cDriver.findElementById("createLeadForm_firstName").sendKeys("Apple");
		
		
		String leadTitle = cDriver.findElementByXPath("//div[@id='sectionHeaderTitle_leads']").getText();
		if(leadTitle.equalsIgnoreCase(cDriver.findElementByXPath("//div[@id='sectionHeaderTitle_leads']").getText())){
			System.out.println("The title is verified: "+leadTitle.toUpperCase());
		}else{
			System.out.println("The title is not verified");
		}
		
		cDriver.findElementByXPath("//input[@name='submitButton']").click();
		String duplicatedLeadName = cDriver.findElementByXPath("//span[@id='viewLead_firstName_sp']").getText();
		if(duplicatedLeadName.equalsIgnoreCase(firstLeadsFirstName)){
			System.out.println("The duplicated name is same as the name present in First Leads page: "+duplicatedLeadName.toUpperCase());
		}else{
			System.out.println("The duplicated name is found incorrect: "+duplicatedLeadName.toUpperCase());
		}
		
		cDriver.close();
		
}

}
