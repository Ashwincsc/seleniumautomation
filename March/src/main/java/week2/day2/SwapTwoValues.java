package week2.day2;

public class SwapTwoValues {
	
public static void main(String args[]){
		
		int a = 2;
		int b = 3;
		
	//	5 = 2 + 3
		a = a + b;
	
	//  2 = 5 - 3	
		b = a - b;
		
	//	3 = 5 - 2
		a = a - b;
				
		System.out.println("The value of 'a' after swap is: "+a);
		System.out.println("The value of 'b' after swap is: "+b);
		
	}

}
