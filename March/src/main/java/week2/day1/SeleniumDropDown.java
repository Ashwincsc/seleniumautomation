package week2.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumDropDown {
	
public static void main (String[] args) throws InterruptedException {
		
		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		ChromeDriver cDriver = new ChromeDriver();
		
		// Launch a website in the chrome browser
		cDriver.get("http://leaftaps.com/opentaps");
		
		// Maximize the chrome browser
		cDriver.manage().window().maximize();
		
		// Form details
		cDriver.findElementById("username").sendKeys("Demosalesmanager");
		cDriver.findElementById("password").sendKeys("crmsfa");
		cDriver.findElementByClassName("decorativeSubmit").click();
		cDriver.findElementByLinkText("CRM/SFA").click();
		cDriver.findElementByLinkText("Create Lead").click();
		cDriver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		cDriver.findElementById("createLeadForm_firstName").sendKeys("Ashwin");
		
		WebElement elementSource = cDriver.findElementById("createLeadForm_dataSourceId");
		Select sourceDropDown = new Select(elementSource);
		sourceDropDown.selectByValue("LEAD_CONFERENCE");
		
		cDriver.findElementById("createLeadForm_firstNameLocal").sendKeys("Ashwin");
		cDriver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");
		cDriver.findElementById("createLeadForm_generalProfTitle").sendKeys("Welcome Lead");
		cDriver.findElementById("createLeadForm_annualRevenue").sendKeys("500000");
		
		WebElement elementIndustry = cDriver.findElementById("createLeadForm_industryEnumId");
		Select dropDown = new Select(elementIndustry);
		dropDown.selectByValue("IND_FINANCE");
		
		WebElement elementOwnership = cDriver.findElementById("createLeadForm_ownershipEnumId");
		Select ownershipDropDown = new Select(elementOwnership);
		ownershipDropDown.deselectByValue("OWN_PARTNERSHIP");
		
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		cDriver.findElementById("").sendKeys("");
		
		
		
		
		cDriver.findElementById("createLeadForm_lastName").sendKeys("Ashwin");
		
		/*
		 * //To get the values present in the field INDUSTRY drop down WebElement
		 * elementIndustry = cDriver.findElementById("createLeadForm_industryEnumId");
		 * Select dropDown = new Select(elementIndustry);
		 * dropDown.selectByValue("IND_FINANCE"); List<WebElement> options =
		 * dropDown.getOptions(); for (WebElement singleElement : options) { String
		 * textValue = singleElement.getText(); System.out.println(textValue); }
		 * 
		 * cDriver.findElementByClassName("smallSubmit").click();
		 * System.out.println("The title of the page is: "+cDriver.getTitle());
		 */
		
		
		//Thread.sleep(3000);
		//cDriver.close();
		
	}

}
