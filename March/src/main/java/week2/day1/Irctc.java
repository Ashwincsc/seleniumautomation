package week2.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {
	
	public static void main(String[] args) throws InterruptedException {
		
				// To launch a chrome browser
				System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
				ChromeDriver cDriver = new ChromeDriver();
				
				// Launch a website in the chrome browser
				cDriver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
				
				// Maximize the chrome browser
				cDriver.manage().window().maximize();
				
				cDriver.findElementById("userRegistrationForm:userName").sendKeys("ash259412");
				cDriver.findElementById("userRegistrationForm:password").sendKeys("ash259412");
				cDriver.findElementById("userRegistrationForm:confpasword").sendKeys("ash259412");
				
				WebElement elementSecQuestion = cDriver.findElementById("userRegistrationForm:securityQ");
				Select secQuestionDropDown = new Select(elementSecQuestion);
				secQuestionDropDown.selectByVisibleText("What is your pet name?");
				
				cDriver.findElementById("userRegistrationForm:securityAnswer").sendKeys("zandu");
				
				WebElement elementPreferredLang = cDriver.findElementByName("userRegistrationForm:prelan");
				Select preferredLangDropDown = new Select(elementPreferredLang);
				preferredLangDropDown.selectByVisibleText("English");
				
				cDriver.findElementById("userRegistrationForm:firstName").sendKeys("Ashwin");
				cDriver.findElementById("userRegistrationForm:lastName").sendKeys("Balasubramani");
				cDriver.findElementById("userRegistrationForm:gender:0").click();
				cDriver.findElementById("userRegistrationForm:maritalStatus:1").click();
				
				WebElement elementDay = cDriver.findElementById("userRegistrationForm:dobDay"); 
				Select dayDropDown = new Select(elementDay);
				dayDropDown.selectByVisibleText("21");
				
				WebElement elementMonth = cDriver.findElementById("userRegistrationForm:dobMonth"); 
				Select monthDropDown = new Select(elementMonth);
				monthDropDown.selectByVisibleText("JAN");
				
				WebElement elementYear = cDriver.findElementById("userRegistrationForm:dateOfBirth"); 
				Select yearDropDown = new Select(elementYear);
				yearDropDown.selectByVisibleText("1989");
				
				WebElement elementOccupation = cDriver.findElementById("userRegistrationForm:occupation"); 
				Select occupationDropDown = new Select(elementOccupation);
				occupationDropDown.selectByVisibleText("Professional");
				
				cDriver.findElementById("userRegistrationForm:uidno").sendKeys("1234567812345678");
				cDriver.findElementById("userRegistrationForm:idno").sendKeys("VRRFA0475B");
				
				WebElement elementCountry = cDriver.findElementById("userRegistrationForm:countries");
				Select countryDropDown = new Select(elementCountry);
				countryDropDown.selectByVisibleText("India");
				
				cDriver.findElementById("userRegistrationForm:email").sendKeys("ash3692000@gmail.com");
				cDriver.findElementById("userRegistrationForm:mobile").sendKeys("9965546369");
				
				WebElement elementNationality = cDriver.findElementById("userRegistrationForm:nationalityId");
				Select nationalityDropDown = new Select(elementNationality);
				nationalityDropDown.selectByVisibleText("India");
				
				cDriver.findElementById("userRegistrationForm:address").sendKeys("9/946 B");
				cDriver.findElementById("userRegistrationForm:street").sendKeys("Sivaraman avenue sivaraman nagar");
				cDriver.findElementById("userRegistrationForm:area").sendKeys("Medavakkam");
				cDriver.findElementById("userRegistrationForm:pincode").sendKeys("600100", Keys.TAB);
				Thread.sleep(3000);
				
				WebElement elementCity = cDriver.findElementById("userRegistrationForm:cityName");
				Select cityDropDown = new Select(elementCity);
				cityDropDown.selectByVisibleText("Kanchipuram");
				Thread.sleep(3000);
				
				WebElement elementPostOffice = cDriver.findElementById("userRegistrationForm:postofficeName");
				Select postOfficeDropDown = new Select(elementPostOffice);
				postOfficeDropDown.selectByVisibleText("Medavakkam  S.O");
				
				cDriver.findElementById("userRegistrationForm:landline").sendKeys("044240369");
				cDriver.findElementById("userRegistrationForm:resAndOff:1").click();
				
				Thread.sleep(5000);
				cDriver.close();				
		
	}

}


