package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumLogin {
	
	public static void main (String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		ChromeDriver cDriver = new ChromeDriver();
		cDriver.get("http://leaftaps.com/opentaps");
		cDriver.manage().window().maximize();
		cDriver.findElementById("username").sendKeys("Demosalesmanager");
		cDriver.findElementById("password").sendKeys("crmsfa");
		cDriver.findElementByClassName("decorativeSubmit").click();
		cDriver.findElementByLinkText("CRM/SFA").click();
		cDriver.findElementByLinkText("Create Lead").click();
		cDriver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		cDriver.findElementById("createLeadForm_firstName").sendKeys("Ashwin");
		cDriver.findElementById("createLeadForm_lastName").sendKeys("Ashwin");
		cDriver.findElementByClassName("smallSubmit").click();
		System.out.println("The title of the page is: "+cDriver.getTitle());
		Thread.sleep(3000);
		cDriver.close();
		
	}
}
