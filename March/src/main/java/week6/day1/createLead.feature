Feature: Create Lead Login

Background:
Given Open the browser
And Max Browser
And Load the URL

Scenario Outline: Positive Flow For Create Lead

And Enter the username as <uName>
And Enter the password as <passWord>
And Click on Login Button
And Click on CRMSFA
And Click on Leads Hyperlink
And Click on Create Lead Hyperlink
And Enter the CompanyName as AquaFix
And Enter the FirstName as Elon
And Enter the LastName as Musk
And Click on Create Lead button
When Verify a lead is created
Then Close the browser

Examples:

|uName|passWord|
|DemoSalesManager|crmsfa|
