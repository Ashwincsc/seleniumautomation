package week6.day1;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/main/java/week6/day1/createLead.feature", glue = "week6.day1", monochrome = true)
public class RunTest extends AbstractTestNGCucumberTests{

}
