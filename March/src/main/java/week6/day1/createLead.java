package week6.day1;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;

public class createLead {

	public ChromeDriver cDriver;

	@And("Open the browser")
	public void launchBrowser() {

		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		cDriver = new ChromeDriver();
	}

	@And("Max Browser")
	public void maximizeBrowser() {

		cDriver.manage().window().maximize();	
	}

	@And("Load the URL")
	public void openLeafTaps() {

		cDriver.get("http://leaftaps.com/opentaps");
	}

	@And("Enter the username as (.*)")
	public void enterUserName(String uName) {

		cDriver.findElementById("username").sendKeys(uName);
	}

	@And("Enter the password as (.*)")
	public void enterPassword(String passWord) {

		cDriver.findElementById("password").sendKeys(passWord);
	}

	@And("Click on Login Button")
	public void clickLogin() {

		cDriver.findElementByClassName("decorativeSubmit").click();
	}

	@And("Click on CRMSFA")
	public void clickCRMSFA() {

		cDriver.findElementByLinkText("CRM/SFA").click();
	}

	@And("Click on Leads Hyperlink")
	public void clickLeadsHyperLink() {

		cDriver.findElementByLinkText("Leads").click();
	}

	@And("Click on Create Lead Hyperlink")
	public void clickCreateLeadHyperLink() {

		cDriver.findElementByLinkText("Create Lead").click();
	}

	@And("Enter the CompanyName as (.*)")
	public void enterCompanyName(String companyName) {

		cDriver.findElementById("createLeadForm_companyName").sendKeys(companyName);
	}

	@And("Enter the FirstName as (.*)")
	public void enterFirstName(String firstName) {

		cDriver.findElementById("createLeadForm_firstName").sendKeys(firstName);
	}

	@And("Enter the LastName as (.*)")
	public void enterLastName(String lastName) {

		cDriver.findElementById("createLeadForm_lastName").sendKeys(lastName);
	}

	@And("Click on Create Lead button")
	public void clickSubmitButton() {

		cDriver.findElementByName("submitButton").click();
	}

	@And("Verify a lead is created")
	public void verifyCreateLead() {
		System.out.println("A lead is created");
	}
	
	@And("Close the browser")
	public void closeBrowser() {
		cDriver.close();
	}
}


