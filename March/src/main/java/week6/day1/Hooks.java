package week6.day1;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	@Before
	public void before(Scenario scenario) {
		System.out.println("Name --> "+scenario.getName());
		System.out.println("Id --> "+scenario.getId());
	}

	@After
	public void after(Scenario scenario) {
		System.out.println("Status --> "+scenario.getStatus());
	}

}
