package week4.day1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindows {
	
	public static void main(String[] args) throws InterruptedException {
		
		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		ChromeDriver cDriver = new ChromeDriver();
		
		// Launch a website in the chrome browser
		cDriver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		// Maximize the chrome browser
		cDriver.manage().window().maximize();
		
		cDriver.findElementByXPath("//a[text()='Contact Us']").click();
		
		Set<String> allWindows = cDriver.getWindowHandles();
		List<String> firstWindow = new ArrayList<>(allWindows);
		cDriver.switchTo().window(firstWindow.get(1));
		
		String verifyName = cDriver.findElementByXPath("//*[text()[contains(.,'New Delhi')]]").getText();
		if(verifyName.contains("New Delhi")){
			System.out.println("The name is verified");
		}else{
			System.out.println("The name is not verified");
		}
		
		cDriver.switchTo().window(firstWindow.get(0));
		cDriver.findElementById("userRegistrationForm:userName").sendKeys("ashwin");
		cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		cDriver.close();
		
	}

}
