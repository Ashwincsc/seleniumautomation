	package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class LearnAlerts {
	
public static void main(String[] args) throws InterruptedException {
	
	System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
	ChromeOptions options = new ChromeOptions();
	options.addArguments("--disable-notifications");
	ChromeDriver cDriver = new ChromeDriver(options);
	
	// Launch a website in the chrome browser
	cDriver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
	
	// Maximize the chrome browser
	cDriver.manage().window().maximize();
	
	WebElement elementFrame = cDriver.findElementByXPath("//iframe[@id='iframeResult']");
	cDriver.switchTo().frame(elementFrame);
	cDriver.findElementByXPath("//button[text()='Try it']").click();
	
	//cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	Thread.sleep(3000);
	
	Alert alertPopUp = cDriver.switchTo().alert();
	alertPopUp.sendKeys("Ashwin");
	alertPopUp.accept();
	
	String name = cDriver.findElementByXPath("//p[@id='demo']").getText();
	System.out.println("The name is -- "+name);
	if(name.contains("Ashwin")) {
		System.out.println("The name is correct");
	}else {
		System.out.println("The name is incorrect");
	}
	
	//cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	Thread.sleep(3000);
	cDriver.close();
}
	

}
