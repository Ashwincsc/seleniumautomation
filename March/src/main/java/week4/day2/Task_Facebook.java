package week4.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Task_Facebook {
	
	public static void main(String[] args) throws InterruptedException {
		
		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver cDriver = new ChromeDriver(options);
				
		// Maximize the chrome browser
		cDriver.manage().window().maximize();
		//cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Thread.sleep(3000);
		// Launch a website in the chrome browser
		cDriver.get("https://www.facebook.com/");
		cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		cDriver.findElementById("email").clear();
		cDriver.findElementById("email").sendKeys("ash3692000@gmail.com");
		cDriver.findElementById("pass").clear();
		cDriver.findElementById("pass").sendKeys("bashwincse100");
		cDriver.findElementById("loginbutton").click();
		cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		cDriver.findElementByXPath("//input[@data-testid='search_input']").click();
		cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		cDriver.findElementByXPath("//input[@data-testid='search_input']").sendKeys("TestLeaf");
		cDriver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		String name = cDriver.findElementByXPath("//button[text()='Like']/preceding::a[text()='TestLeaf']").getText();
		if(name.contains("TestLeaf")) {
			System.out.println("The name '"+name+"' is verified");
		}else {
			System.out.println("The name '"+name+"' is not verified");
		}
		
		String likeButtonName = cDriver.findElementByXPath("//button[text()='Like']").getText();
		if(likeButtonName.equalsIgnoreCase("Like")) {
			System.out.println("The like button name is verified");
			cDriver.findElementByXPath(" //button[text()='Like']").click();
			cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			cDriver.findElementByXPath("//a[text()='Close']").click();
			
			cDriver.findElementByXPath("//button[text()='Like']/preceding::a[text()='TestLeaf']").click();
			cDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		}else if(likeButtonName.equalsIgnoreCase("Liked")) {
			System.out.println("The page is already liked");
			cDriver.findElementByXPath(" //button[text()='Like']").click();
		}
		String title = cDriver.getTitle();
		System.out.println("The title is -- "+title);
		if(title.contains("TestLeaf")) {
			String likeCount = cDriver.findElementByXPath("//span[text()='Community']/following::div[contains(text(),'people like this')]").getText();
			System.out.println(likeCount);
		}
		cDriver.close();
	}

}
