package week4.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task_ZoomCar {
	
	public static void main(String[] args) throws InterruptedException {
		
		// To launch a chrome browser
		System.setProperty("webdriver.chrome.driver", "/Users/ashwin/eclipse-workspace/March/drivers/chromedriver");
		ChromeDriver cDriver = new ChromeDriver();
		
		// Launch a website in the chrome browser
		cDriver.get("https://www.zoomcar.com/chennai/");
		
		// Maximize the chrome browser
		cDriver.manage().window().maximize();	
		
		cDriver.findElementByXPath("//a[text()='Start your wonderful journey']").click();
		cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		cDriver.findElementByXPath("//div[text()='Popular Pick-up points']//following::div[1]").click();
		cDriver.findElementByXPath("//button[text()='Next']").click();
		cDriver.findElementByXPath("//div[text()='Select a month']//following::div[3]").click();
		cDriver.findElementByXPath("//div[text()='Start date ']/following::div[text()='Mon']").click();
		cDriver.findElementByXPath("//button[text()='Next']").click();
		//cDriver.findElementByXPath("//div[@class='month picked']/following::div[1]").click();
		//cDriver.findElementByXPath("//div[text()='Start date ']/following::div[6]").click();
		
		cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		String timeStamp = cDriver.findElementByXPath("//div[text()='PICKUP TIME']/following::div[2]").getText(); 
		System.out.println("The starting date is: "+timeStamp);
		cDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		cDriver.findElementByXPath("//button[text()='Done']").click();
		cDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		List<WebElement> totalNumberOfCars = cDriver.findElementsByXPath("//div[@class='price']");
		List<String> eachCarPrice = new ArrayList<>();
		System.out.println("The total number of cars that are available are: "+totalNumberOfCars.size());
		
		for (WebElement eachCar : totalNumberOfCars) {
			String text = eachCar.getText().replaceAll("\\D", "");
			eachCarPrice.add(text);
		}
		
		String max = Collections.max(eachCarPrice);
		String carName = cDriver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]").getText();
		System.out.println("The car with highest price is: "+carName+" -- "+max);
		cDriver.findElementByXPath("//div[contains(text(),'"+max+"')]/following::button[1]").click();
		cDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		cDriver.close();
	}

}
